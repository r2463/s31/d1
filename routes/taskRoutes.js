
const express = require('express');

const router = express.Router();

const taskController = require('../controllers/taskControllers')


// ADD A TASK
router.post('/', async (req, res) => {
	// console.log(req.body)	//object

	await taskController.createTask(req.body).then(result => res.send(result))
})


// GET ALL TASKS
router.get('/', async (req, res) => {

	try{
		await taskController.getAllTasks().then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})

// DELETE A TASK
router.delete('/:id/delete', async (req, res) => {

	// console.log(req)

	try{
		await taskController.deleteTask(req.params.id).then(response => res.send(response))

	} catch(err){
		res.status(500).json(error)
	}
})


// UPDATE A TASK

// USING findByIdAndDelete

router.put('/:id', async (req, res) => {
	// console.log(req.params.id)
	const id = req.params.id;
	// console.log(req.body)

	await taskController.updateTask(id, req.body).then(response => res.send(response))
})

//  USING findOneAndDelete
// router.put('/', async (req, res) => {

// 	await taskController.updateTask(req.body.name, req.body).then(response => res.send(response))
// })


// GET A SPECIFIC TASK
router.get('/:id', async (req, res) => {

	const id = req.params.id;

	try{

	await taskController.getOneTask(req.params.id).then(response => res.send(response))
		
	}catch(err){
			res.status(500).json(err)
		}
})



// UPDATE A TASK TO "COMPLETE"

router.put('/:id/complete', async (req, res) => {
	

	try{
		await taskController.completeTask(req.params.id).then(result => res.send(result))
	
	} catch(err){
		res.status(500).json(err)
	}
})




module.exports = router;